var underBustSize;
var overBustSize;
var breastSize;
var breastSizeAlphabet;

$('button').click(function () {
    underBustSize = Number(document.getElementById('underBustSize').value);
    overBustSize = Number(document.getElementById('overBustSize').value);
    if (underBustSize >= 68 && underBustSize <= 72) {
        breastSize = 32;
        breastSizeAlphabet = bustAlphabetForSize32(overBustSize);
        
    } 
    else if (underBustSize >= 73 && underBustSize <= 77) {
        breastSize = 34;
        breastSizeAlphabet = bustAlphabetForSize34(overBustSize);
    } 
    else if (underBustSize >= 78 && underBustSize <= 82) {
        breastSize = 36;
        breastSizeAlphabet = bustAlphabetForSize36(overBustSize);
    } 
    else if (underBustSize >= 83 && underBustSize <= 87) {
        breastSize = 38;
        breastSizeAlphabet = bustAlphabetForSize38(overBustSize);
    } 
    else if (underBustSize >= 88 && underBustSize <= 92) {
        breastSize = 40;
        breastSizeAlphabet = bustAlphabetForSize40(overBustSize);
    } 
    else if (underBustSize >= 93 && underBustSize <= 97) {
        breastSize = 42;
        breastSizeAlphabet = bustAlphabetForSize42(overBustSize);
    } 
    else if (underBustSize >= 98 && underBustSize <= 102) {
        breastSize = 44;
        breastSizeAlphabet = bustAlphabetForSize44(overBustSize);
    }
    
    else {
        var noBraValue = false;
    }
    
    if (noBraValue != false){
        alert("Your Bra Size is: " + breastSize + breastSizeAlphabet);
    }
    else {
        alert("Unfortunately we are unable to determine your bra size.");
    }
});

function bustAlphabetForSize32(overBustSize) {
    var alphabet;
    if (overBustSize >= 82 && overBustSize <= 84) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 84 && overBustSize <= 86) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 86 && overBustSize <= 88) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 88 && overBustSize <= 90) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 90 && overBustSize <= 92) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 92 && overBustSize <= 94) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 94 && overBustSize <= 96) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 96 && overBustSize <= 98) {
        alphabet = 'H';
    }
    return alphabet;
}

function bustAlphabetForSize34(overBustSize) {
    if (overBustSize >= 87 && overBustSize <= 89) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 89 && overBustSize <= 91) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 91 && overBustSize <= 93) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 93 && overBustSize <= 96) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 97 && overBustSize <= 100) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 101 && overBustSize <= 104) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 105 && overBustSize <= 107) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 108 && overBustSize <= 111) {
        alphabet = 'H';
    }
    return alphabet;
}

function bustAlphabetForSize36(overBustSize) {
    if (overBustSize >= 92 && overBustSize <= 94) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 94 && overBustSize <= 96) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 96 && overBustSize <= 98) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 98 && overBustSize <= 101) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 102 && overBustSize <= 105) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 106 && overBustSize <= 109) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 110 && overBustSize <= 113) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 114 && overBustSize <= 117) {
        alphabet = 'H';
    }
    return alphabet;
}

function bustAlphabetForSize38(overBustSize) {
    if (overBustSize >= 97 && overBustSize <= 99) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 99 && overBustSize <= 101) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 101 && overBustSize <= 103) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 103 && overBustSize <= 106) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 106 && overBustSize <= 110) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 111 && overBustSize <= 114) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 115 && overBustSize <= 118) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 119 && overBustSize <= 122) {
        alphabet = 'H';
    }
    return alphabet;
}

function bustAlphabetForSize40(overBustSize) {
    if (overBustSize >= 102 && overBustSize <= 104) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 104 && overBustSize <= 106) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 106 && overBustSize <= 108) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 108 && overBustSize <= 111) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 112 && overBustSize <= 115) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 116 && overBustSize <= 119) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 120 && overBustSize <= 123) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 124 && overBustSize <= 127) {
        alphabet = 'H';
    }
    return alphabet;
}
function bustAlphabetForSize42(overBustSize) {
    if (overBustSize >= 107 && overBustSize <= 109) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 109 && overBustSize <= 111) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 111 && overBustSize <= 113) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 113 && overBustSize <= 116) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 117 && overBustSize <= 120) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 121 && overBustSize <= 124) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 125 && overBustSize <= 128) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 129 && overBustSize <= 132) {
        alphabet = 'H';
    }
    return alphabet;
}

function bustAlphabetForSize44(overBustSize) {
    if (overBustSize >= 112 && overBustSize <= 114) {
        alphabet = 'A';
    } 
    else if (overBustSize >= 114 && overBustSize <= 116) {
        alphabet = 'B';
    } 
    else if (overBustSize >= 116 && overBustSize <= 118) {
        alphabet = 'C';
    } 
    else if (overBustSize >= 118 && overBustSize <= 122) {
        alphabet = 'D';
    } 
    else if (overBustSize >= 123 && overBustSize <= 126) {
        alphabet = 'DD';
    } 
    else if (overBustSize >= 127 && overBustSize <= 130) {
        alphabet = 'F';
    } 
    else if (overBustSize >= 131 && overBustSize <= 134) {
        alphabet = 'G';
    } 
    else if (overBustSize >= 135 && overBustSize <= 138) {
        alphabet = 'H';
    }
    return alphabet;
}

