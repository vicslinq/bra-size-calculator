# README #
Implementation of a Bra size calculator using the under and over bust size measurements in centimetres. Implemented in JavaScript

### What is this repository for? ###

* To be used as part of an e-commerce solution
* Version 1.0
* Temporarily available at: http://bracalc.haail.co

### How do I get set up? ###

* Open the index.html file in any modern web browser

### Who do I talk to? ###

* Algorithm Author: victorobaitor@gmail.com
* Algorithm Coder: fredobaseki@gmail.com